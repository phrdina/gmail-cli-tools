#!/usr/bin/env python3

import argparse
import httplib2
import configparser
import json
import os
import sys

from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_httplib2 import AuthorizedHttp
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient import discovery


def get_creds(creds_file):
    creds = Credentials.from_authorized_user_file(creds_file)

    if creds.expired:
        creds.refresh(Request())

    return creds


def get_service(creds):
    http = httplib2.Http()
    http.redirect_codes = http.redirect_codes - {308}

    http = AuthorizedHttp(creds, http)

    return discovery.build('gmail', 'v1', http=http)


def get_labels(service):
    result = service.users().labels().list(userId='me').execute()
    labels = result.get('labels', [])

    labelsByName = {}
    labelsById = {}

    for label in labels:
        labelsByName[label['name']] = label
        labelsById[label['id']] = label

    return labelsById, labelsByName


def labelIdToName(labels, ids):
    ret = []
    for label in labels:
        ret.append(ids[label]['name'])
    return ret


def labelNameToId(labels, names):
    ret = []
    for label in labels:
        ret.append(names[label]['id'])
    return ret


def get_filters(service, wrap=False):
    result = service.users().settings().filters().list(userId='me').execute()
    filters = result.get('filter', [])

    ids, names = get_labels(service)

    ret = []

    for filtr in filters:
        if 'action' in filtr:
            addLabels = filtr['action'].get('addLabelIds', [])
            delLabels = filtr['action'].get('removeLabelIds', [])
        else:
            addLabels = []
            delLabels = []

        item = {
            'labels': labelIdToName(addLabels, ids),
            'not_labels': labelIdToName(delLabels, ids),
            'match': filtr['criteria'].get('query', ''),
            'not_match': filtr['criteria'].get('negatedQuery', ''),
        }

        if wrap:
            item = {
                'id': filtr['id'],
                'data': item,
            }

        ret.append(item)

    return ret


def compareFilter(a, b):
    if a['match'] != b['match']:
        return False

    if a['not_match'] != b['not_match']:
        return False

    if a['labels'].sort() != b['labels'].sort():
        return False

    if a['not_labels'].sort() != b['not_labels'].sort():
        return False

    return True


def filterExists(filtr, filters):
    for f in filters:
        if compareFilter(f, filtr):
            return True

    return False


def set_filters(service, filters):
    curFilters = get_filters(service)
    ids, names = get_labels(service)

    count = 0

    for filtr in filters:
        if filterExists(filtr, curFilters):
            continue

        newFilter = {
            'action': {
                'addLabelIds': labelNameToId(filtr['labels'], names),
                'removeLabelIds': labelNameToId(filtr['not_labels'], names),
            },
            'criteria': {
                'query': filtr['match'],
                'negatedQuery': filtr['not_match'],
            },
        }

        result = service.users().settings().filters().create(
            userId='me',
            body=newFilter,
        ).execute()

        count += 1

    print('filters added: {0}'.format(count))


def prune_filters(service, filters):
    curFilters = get_filters(service, wrap=True)

    count = 0

    for filtr in curFilters:
        if filterExists(filtr['data'], filters):
            continue

        result = service.users().settings().filters().delete(
            userId='me',
            id=filtr['id'],
        ).execute()

        count += 1

    print('filters removed: {0}'.format(count))


def action_auth(__, config):
    scope = [
        'https://mail.google.com/',
        'https://www.googleapis.com/auth/gmail.labels',
        'https://www.googleapis.com/auth/gmail.settings.basic',
    ]
    flow = None

    secret_file = os.path.expanduser(config['secret'])
    creds_file = os.path.expanduser(config['creds'])

    with open(secret_file) as file:
        flow = InstalledAppFlow.from_client_config(json.load(file), scope)

    flow.run_console()

    with open(creds_file, 'w') as file:
        file.write(flow.credentials.to_json())


def action_token(__, config):
    creds_file = os.path.expanduser(config['creds'])
    print(get_creds(creds_file).token)


def action_filter(args, config):
    creds_file = os.path.expanduser(config['creds'])
    filter_file = config.get('filters', None)

    if filter_file is None:
        print('missing filters for {} in configfile'.format(args.name))
        sys.exit(1)

    filter_file = os.path.expanduser(filter_file)

    creds = get_creds(creds_file)
    service = get_service(creds)

    if args.action == 'fetch':
        filters = get_filters(service)

        with open(filter_file, 'w') as file:
            json.dump(filters, file, indent=2)
    elif args.action == 'push':
        with open(filter_file) as file:
            filters = json.load(file)

        set_filters(service, filters)
    elif args.action == 'prune':
        with open(filter_file) as file:
            filters = json.load(file)

        prune_filters(service, filters)
    else:
        print('unknown action {0}'.format(args.action))
        sys.exit(1)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config',
                        help='path to configuration file (default is ~/.gmail-cli.conf)')

    subparsers = parser.add_subparsers(metavar='ACTION')
    subparsers.required = True

    authparser = subparsers.add_parser('auth', help='run authentication')
    authparser.set_defaults(func=action_auth)
    authparser.add_argument('name', help='service name from configuration file')

    tokenparser = subparsers.add_parser('token', help='get token')
    tokenparser.set_defaults(func=action_token)
    tokenparser.add_argument('name', help='service name from configuration file')

    filterparser = subparsers.add_parser('filter', help='manage gmail filters')
    filterparser.set_defaults(func=action_filter)
    filterparser.add_argument('-n', '--dry-run', action='store_true', default=False,
                              help='do not make any API calls to gmail')
    filterparser.add_argument('name', help='service name from configuration file')
    filterparser.add_argument('action', help='action to do')

    return parser.parse_args()


def parse_config(args):
    configfile = args.config
    if not configfile:
        configfile = os.path.join(os.path.expanduser('~'), '.gmail-cli.conf')

    if not os.path.exists(configfile):
        print('config file {} does not exist'.format(configfile))
        sys.exit(1)

    config = configparser.ConfigParser()
    config.read(configfile)

    if args.name not in config:
        print('there is no config for {} in configfile {}'.format(args.name, configfile))
        sys.exit(1)

    ret = config[args.name]

    if 'secret' not in ret:
        print('missing secret for {} in configfile {}'.format(args.name, configfile))

    if 'creds' not in ret:
        print('missing creds for {} in configfile {}'.format(args.name, configfile))

    return ret


def main():
    args = parse_args()
    config = parse_config(args)

    args.func(args, config)


if __name__ == '__main__':
    main()
