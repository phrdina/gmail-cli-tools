gmail-cli-tools
===============

Tool to manage xoauth2 google authentication and gmail filters.

There are multiple tools with various dependencies or using deprecated
libraries which motivated me to create a single tool to manage what
I need to use mbsync and mutt with gmail.

Required dependencies
---------------------

- https://github.com/googleapis/google-api-python-client
- https://github.com/googleapis/google-auth-library-python
- https://github.com/googleapis/google-auth-library-python-httplib2
- https://github.com/googleapis/google-auth-library-python-oauthlib

On Fedora you can run this command to satisfy the dependencies ::

  dnf install python3-google-api-client \
              python3-google-auth \
              python3-google-auth-httplib2 \
              python3-google-auth-oauthlib

Initial setup
-------------

In order to use this tool the user needs to create a `google console`_ project
where it is possible to define xoauth2 client which is necessary to get
a JSON authentication configuration. All the necessary files needs to be
set in configuration file. The default path is ~/.gmail-cli.conf, it can
be changed using ``-c`` option.

With the JSON file it is possible to authenticate this tool with your gmail
account ::

  ./gmail-cli.py auth service_name

Usage
-----

To get a token for your application simply run ::

  ./gmail-cli.py token service_name

To download all your filters from gmail run ::

  ./gmail-cli.py filter service_name fetch

Now you can edit your filters and once you are satisfied with the result
you can push the changes and new filter to gmail using ::

  ./gmail-cli.py filter service_name push

If some filters were removed from the JSON filter file they will not be
removed from gmail, in order to do that run ::

  ./gmail-cli.py filter service_name prune

.. _google console: https://console.developers.google.com/
